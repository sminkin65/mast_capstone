import 'package:MAST_app/Class/Model/ClassRoom.dart';
import 'package:MAST_app/Student/Analysis/AnalysisArgs.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import './login.dart';
import './home.dart';
import './displayerror.dart';
import './loadinginit.dart';
import './loadingauth.dart';
import './Class/CreateClassPage.dart';
import './JoinClass/JoinClassPage.dart';
import './ViewClasses/ViewClasses.dart';
import './Student/StudentHomePage.dart';
import './Teacher/TeacherHomePage.dart';
import './Student/DataHistory/datahistory.dart';
import './Student/Pronunciation/pronunciation.dart';
import './Student/Analysis/viewanalysis.dart';

void main() { 
  //WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp()); 
}

class MyApp extends StatefulWidget {
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  bool _initialized = false;
  bool _error = false;

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  // Function to initialize FlutterFire instance
  void initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch(e) {
      setState(() {
        _error = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    // If something went wrong, display error
    if (_error) {
      return MaterialApp(
        home: DisplayError()
      );
    }

    // If app is not initialized, display loading
    if (!_initialized) {
      return MaterialApp(
        home: LoadingInit("Loading")
      );
    }

    // If app is initialized, return auth page
    return MaterialApp(
      home: LoadingAuth(),
      onGenerateRoute: _getRoute
    );

  }

  Route<dynamic> _getRoute(RouteSettings settings) {
    WidgetBuilder builder;

    switch (settings.name) {
      case '/login':
        builder = (context) => Login();
        break;
      case '/welcome':
        builder = (context) => Home();
        break;
      case '/createclass':
        builder = (context) => CreateClassPage();
        break;
      case '/viewclasses':
        builder = (context) => ViewClasses();
        break;
      case '/joinclass':
        builder = (context) => JoinClassPage();
        break;
      case '/teacher':
        builder = (context) => TeacherHome();
        break;              
      case '/student':
        final ClassRoom args = settings.arguments;
        builder = (context) => StudentHome(args);
        break;
      case '/datahistory':
        final ClassRoom args = settings.arguments;
        builder = (context) => DataHistoryPage(args);
        break;
      case '/pronunciation':
        final ClassRoom args = settings.arguments;
        builder = (context) => PronunciationPage(args);
        break;
      case '/viewanalysis':
        final AnalysisArgs args = settings.arguments;
        builder = (context) => ViewAnalysis(args);
        break;         
      default:
        throw new Exception('Invalid Route: ${settings.name}');
    }

    return MaterialPageRoute(builder: builder, settings: settings);    
  }
  
}